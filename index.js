let number=2;
const getCube = (number) => number ** 3;
let cubeOf = getCube(number);
console.log(`Result of ${number} is ${cubeOf}`);

const address = ["258 Washington Ave NW", "California", 90011];
console.log(`I live at ${address[0]} ${address[1]},${address[2]}`);
const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	ft:20,
	inch:3
};
console.log(`${animal.name} is a ${animal.type}. He weight at ${animal.weight} with a measurement of ${animal.ft} ft ${animal.inch} in.`); 

let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => {
	console.log(`${number}`);
})
let reduceNumber = numbers.reduce((previousValue, currentValue)=>previousValue + currentValue);
console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);